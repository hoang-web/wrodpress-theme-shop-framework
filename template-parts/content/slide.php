<div class="iner-product" id="<?php the_ID()?>">
    <div class="img">
    <a class="img hover-zoom"   href="<?php the_permalink();?>" title="<?php the_title();?>">
        <?php the_post_thumbnail( 'thumbnail' ); ?>
            
        </a> 
    </div>
    <div class="content-slider-product">
          <h2>
                <a class="product-title" href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                <?php the_title() ?>
                </a>
          </h2>
    </div>
</div>